﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string last_tab = "up";
        Bitmap bitmap1 = Properties.Resources._50x50; //pers
        Bitmap bitmap2 = Properties.Resources._50x50_2; //pers shooting
        Bitmap bitmap3 = Properties.Resources.bot_50x50; //bot
        Bitmap bitmap4 = Properties.Resources.bot_50x50_2; //bot shooting
        Bitmap bitmap8 = Properties.Resources.bot_50x50_dead; //bot dead
        Bitmap bitmap7 = Properties.Resources._50x50_dead; //pers dead

        int lvl_bot = 100, lvl_bot_shot = 100, bot_bullet_spped = 20, bullet_spped = 20;

        private void gogogo(string now)
        {

            if ((last_tab == "left") && (now == "left")) { }
            if ((last_tab == "left") && (now == "right")) { bitmap7.RotateFlip(RotateFlipType.Rotate180FlipNone); bitmap1.RotateFlip(RotateFlipType.Rotate180FlipNone); bitmap2.RotateFlip(RotateFlipType.Rotate180FlipNone); pictureBox1.BackgroundImage = null; pictureBox1.BackgroundImage = bitmap1; }
            if ((last_tab == "left") && (now == "down")) { bitmap7.RotateFlip(RotateFlipType.Rotate270FlipNone); bitmap1.RotateFlip(RotateFlipType.Rotate270FlipNone); bitmap2.RotateFlip(RotateFlipType.Rotate270FlipNone); pictureBox1.BackgroundImage = null; pictureBox1.BackgroundImage = bitmap1; }
            if ((last_tab == "left") && (now == "up")) { bitmap7.RotateFlip(RotateFlipType.Rotate90FlipNone); bitmap1.RotateFlip(RotateFlipType.Rotate90FlipNone); bitmap2.RotateFlip(RotateFlipType.Rotate90FlipNone); pictureBox1.BackgroundImage = null; pictureBox1.BackgroundImage = bitmap1; }

            if ((last_tab == "right") && (now == "left")) { bitmap7.RotateFlip(RotateFlipType.Rotate180FlipNone); bitmap1.RotateFlip(RotateFlipType.Rotate180FlipNone); bitmap2.RotateFlip(RotateFlipType.Rotate180FlipNone); pictureBox1.BackgroundImage = null; pictureBox1.BackgroundImage = bitmap1; }
            if ((last_tab == "right") && (now == "right")) { }
            if ((last_tab == "right") && (now == "down")) { bitmap7.RotateFlip(RotateFlipType.Rotate90FlipNone); bitmap1.RotateFlip(RotateFlipType.Rotate90FlipNone); bitmap2.RotateFlip(RotateFlipType.Rotate90FlipNone); pictureBox1.BackgroundImage = null; pictureBox1.BackgroundImage = bitmap1; }
            if ((last_tab == "right") && (now == "up")) { bitmap7.RotateFlip(RotateFlipType.Rotate270FlipNone); bitmap1.RotateFlip(RotateFlipType.Rotate270FlipNone); bitmap2.RotateFlip(RotateFlipType.Rotate270FlipNone); pictureBox1.BackgroundImage = null; pictureBox1.BackgroundImage = bitmap1; }

            if ((last_tab == "up") && (now == "left")) { bitmap7.RotateFlip(RotateFlipType.Rotate270FlipNone); bitmap1.RotateFlip(RotateFlipType.Rotate270FlipNone); bitmap2.RotateFlip(RotateFlipType.Rotate270FlipNone); pictureBox1.BackgroundImage = null; pictureBox1.BackgroundImage = bitmap1; }
            if ((last_tab == "up") && (now == "right")) { bitmap7.RotateFlip(RotateFlipType.Rotate90FlipNone); bitmap1.RotateFlip(RotateFlipType.Rotate90FlipNone); bitmap2.RotateFlip(RotateFlipType.Rotate90FlipNone); pictureBox1.BackgroundImage = null; pictureBox1.BackgroundImage = bitmap1; }
            if ((last_tab == "up") && (now == "down")) { bitmap7.RotateFlip(RotateFlipType.Rotate180FlipNone); bitmap1.RotateFlip(RotateFlipType.Rotate180FlipNone); bitmap2.RotateFlip(RotateFlipType.Rotate180FlipNone); pictureBox1.BackgroundImage = null; pictureBox1.BackgroundImage = bitmap1; }
            if ((last_tab == "up") && (now == "up")) { }

            if ((last_tab == "down") && (now == "left")) { bitmap7.RotateFlip(RotateFlipType.Rotate90FlipNone); bitmap1.RotateFlip(RotateFlipType.Rotate90FlipNone); bitmap2.RotateFlip(RotateFlipType.Rotate90FlipNone); pictureBox1.BackgroundImage = null; pictureBox1.BackgroundImage = bitmap1; }
            if ((last_tab == "down") && (now == "right")) { bitmap7.RotateFlip(RotateFlipType.Rotate270FlipNone); bitmap1.RotateFlip(RotateFlipType.Rotate270FlipNone); bitmap2.RotateFlip(RotateFlipType.Rotate270FlipNone); pictureBox1.BackgroundImage = null; pictureBox1.BackgroundImage = bitmap1; }
            if ((last_tab == "down") && (now == "down")) { }
            if ((last_tab == "down") && (now == "up")) { bitmap7.RotateFlip(RotateFlipType.Rotate180FlipNone); bitmap1.RotateFlip(RotateFlipType.Rotate180FlipNone); bitmap2.RotateFlip(RotateFlipType.Rotate180FlipNone); pictureBox1.BackgroundImage = null; pictureBox1.BackgroundImage = bitmap1; }
        }

        bool destroyed = false;
        private void Form1_KeyDown(object sender, KeyEventArgs e) //управление
        {
            if ((e.KeyCode == Keys.A) || ((e.KeyCode == Keys.Left)))
            {
                if (destroyed == false)
                {
                    gogogo("left");
                    last_tab = "left";
                    if ((this.Controls["pictureBox1"].Left - black_square.speed) >= 0)
                        this.Controls["pictureBox1"].Left = this.Controls["pictureBox1"].Left - black_square.speed;
                }
            }

            if ((e.KeyCode == Keys.D) || ((e.KeyCode == Keys.Right)))
            {
                if (destroyed == false)
                {
                    gogogo("right");
                    last_tab = "right";
                    if ((this.Controls["pictureBox1"].Left + this.Controls["pictureBox1"].Size.Width) < (this.Width))
                        this.Controls["pictureBox1"].Left = this.Controls["pictureBox1"].Left + black_square.speed;
                }
            }

            if ((e.KeyCode == Keys.W) || ((e.KeyCode == Keys.Up)))
            {
                if (destroyed == false)
                {
                    gogogo("up");
                    last_tab = "up";
                    if ((this.Controls["pictureBox1"].Top - black_square.speed) >= 0)
                        this.Controls["pictureBox1"].Top = this.Controls["pictureBox1"].Top - black_square.speed;
                }
            }

            if ((e.KeyCode == Keys.S) || ((e.KeyCode == Keys.Down)))
            {
                if (destroyed == false)
                {
                    gogogo("down");
                    last_tab = "down";
                    if ((this.Controls["pictureBox1"].Top + this.Controls["pictureBox1"].Height) < (this.Height))
                        this.Controls["pictureBox1"].Top = this.Controls["pictureBox1"].Top + black_square.speed;
                }
            }

            if (e.KeyCode == Keys.Space)
            {
                if (destroyed == false)
                {
                    bs_strike();
                }
            }
        }

        character black_square = new character();
        private void Form1_Load(object sender, EventArgs e)
        {
            resp();
        }

        private async void bs_shoot_animation()
        {

            pictureBox1.BackgroundImage = bitmap2;
            await TaskEx.Delay(100);
            pictureBox1.BackgroundImage = bitmap1;
        }

        List<string> bs_shots = new List<string>();
        int number_of_shot = 0;
        private async void bs_strike()
        {
            PictureBox bs_shot = new PictureBox();
            bs_shot.Width = 5;
            bs_shot.Height = 5;
            bs_shot.Visible = true;
            bs_shot.Enabled = true;
            bs_shot.BackColor = Color.Black;
            bs_shots.Add("bs_shot" + (number_of_shot).ToString());
            bs_shot.Name = "bs_shot" + (number_of_shot).ToString();
            number_of_shot++;
            Controls.Add(bs_shot);
            bs_shoot_animation();
            if (last_tab == "up")
            {
                bs_shot.Left = this.Controls["pictureBox1"].Left + this.Controls["pictureBox1"].Width / 2;
                bs_shot.Top = this.Controls["pictureBox1"].Top;
                while (bs_shot.Top > 0) { bs_shot.Top = bs_shot.Top - bullet_spped; await TaskEx.Delay(100); }
            }
            if (last_tab == "right")
            {
                bs_shot.Left = this.Controls["pictureBox1"].Left + this.Controls["pictureBox1"].Width;
                bs_shot.Top = this.Controls["pictureBox1"].Top + this.Controls["pictureBox1"].Height / 2;
                while (bs_shot.Left < this.Width) { bs_shot.Left = bs_shot.Left + bullet_spped; await TaskEx.Delay(100); }
            }
            if (last_tab == "down")
            {
                bs_shot.Left = this.Controls["pictureBox1"].Left + this.Controls["pictureBox1"].Width / 2;
                bs_shot.Top = this.Controls["pictureBox1"].Top + this.Controls["pictureBox1"].Height;
                while (bs_shot.Top < this.Height) { bs_shot.Top = bs_shot.Top + bullet_spped; await TaskEx.Delay(100); }
            }
            if (last_tab == "left")
            {
                bs_shot.Left = this.Controls["pictureBox1"].Left;
                bs_shot.Top = this.Controls["pictureBox1"].Top + this.Controls["pictureBox1"].Height / 2;
                while (bs_shot.Left > 0) { bs_shot.Left = bs_shot.Left - bullet_spped; await TaskEx.Delay(100); }
            }

        }

        List<string> bot_shots = new List<string>();                    //стельба бота
        int bot_number_of_shot = 0;
        private void bot_strike(int name_of_bot)
        {
            try
            {                
                PictureBox bot_shot = new PictureBox();
                bot_shot.Width = 5;
                bot_shot.Height = 5;
                bot_shot.Visible = true;
                bot_shot.Enabled = true;
                bot_shot.BackColor = Color.Gray;
                bot_shots.Add("bot_shot" + (bot_number_of_shot).ToString());
                bot_shot.Name = "bot_shot" + (bot_number_of_shot).ToString();
                bot_number_of_shot++;
                Controls.Add(bot_shot);
                if (this.Controls[bots[name_of_bot][0]].Left < this.Controls["pictureBox1"].Left) //way = "right";
                {
                    bots[name_of_bot][1] = "right";
                    bot_shooting(bots[name_of_bot][1], name_of_bot);
                    bot_shot.Left = this.Controls[bots[name_of_bot][0]].Left + this.Controls[bots[name_of_bot][0]].Width;
                    bot_shot.Top = this.Controls[bots[name_of_bot][0]].Top + this.Controls[bots[name_of_bot][0]].Height / 2;
                    bot_bullet_fly(bots[name_of_bot][1], bot_shot.Name);
                }
                if (this.Controls[bots[name_of_bot][0]].Left > this.Controls["pictureBox1"].Left) //way = "left";
                {
                    bots[name_of_bot][1] = "left";
                    bot_shooting(bots[name_of_bot][1], name_of_bot);                    
                    bot_shot.Left = this.Controls[bots[name_of_bot][0]].Left;
                    bot_shot.Top = this.Controls[bots[name_of_bot][0]].Top + this.Controls[bots[name_of_bot][0]].Height / 2;
                    bot_bullet_fly(bots[name_of_bot][1], bot_shot.Name);
                }
                if ((this.Controls[bots[name_of_bot][0]].Left == this.Controls["pictureBox1"].Left) && (this.Controls[bots[name_of_bot][0]].Top < this.Controls["pictureBox1"].Top)) //way = "down";
                {
                    bots[name_of_bot][1] = "down";
                    bot_shooting(bots[name_of_bot][1], name_of_bot);
                    bot_shot.Left = this.Controls[bots[name_of_bot][0]].Left + this.Controls[bots[name_of_bot][0]].Width / 2;
                    bot_shot.Top = this.Controls[bots[name_of_bot][0]].Top + this.Controls[bots[name_of_bot][0]].Height;
                    bot_bullet_fly(bots[name_of_bot][1], bot_shot.Name);
                }
                if ((this.Controls[bots[name_of_bot][0]].Left == this.Controls["pictureBox1"].Left) && (this.Controls[bots[name_of_bot][0]].Top > this.Controls["pictureBox1"].Top)) //way = "up";
                {
                    bots[name_of_bot][1] = "up";
                    bot_shooting(bots[name_of_bot][1], name_of_bot);
                    bot_shot.Left = this.Controls[bots[name_of_bot][0]].Left + this.Controls[bots[name_of_bot][0]].Width / 2;
                    bot_shot.Top = this.Controls[bots[name_of_bot][0]].Top;
                    bot_bullet_fly(bots[name_of_bot][1], bot_shot.Name);
                }
            }
            catch { }
        }

        private async void bot_shooting(string way, int name_of_bot)
        {
            try
            {
                Bitmap bitmap5 = new Bitmap(bitmap3);
                Bitmap bitmap6 = new Bitmap(bitmap4);
                if (way == "left")
                {
                    bitmap5.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    bitmap6.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    this.Controls[bots[name_of_bot][0]].BackgroundImage = bitmap5;
                    await TaskEx.Delay(100);
                    this.Controls[bots[name_of_bot][0]].BackgroundImage = bitmap6;
                    await TaskEx.Delay(100);
                    this.Controls[bots[name_of_bot][0]].BackgroundImage = bitmap5;
                }
                if (way == "right")
                {
                    bitmap5.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    bitmap6.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    this.Controls[bots[name_of_bot][0]].BackgroundImage = bitmap5;
                    await TaskEx.Delay(100);
                    this.Controls[bots[name_of_bot][0]].BackgroundImage = bitmap6;
                    await TaskEx.Delay(100);
                    this.Controls[bots[name_of_bot][0]].BackgroundImage = bitmap5;
                }
                if (way == "down")
                {
                    bitmap5.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    bitmap6.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    this.Controls[bots[name_of_bot][0]].BackgroundImage = bitmap5;
                    await TaskEx.Delay(100);
                    this.Controls[bots[name_of_bot][0]].BackgroundImage = bitmap6;
                    await TaskEx.Delay(100);
                    this.Controls[bots[name_of_bot][0]].BackgroundImage = bitmap5;
                }
                if (way == "up")
                {
                    this.Controls[bots[name_of_bot][0]].BackgroundImage = bitmap5;
                    await TaskEx.Delay(100);
                    this.Controls[bots[name_of_bot][0]].BackgroundImage = bitmap6;
                    await TaskEx.Delay(100);
                    this.Controls[bots[name_of_bot][0]].BackgroundImage = bitmap5;
                }
            }
            catch { }
        }

        private async void bot_bullet_fly (string way, string name_of_shot)
        {
            try
            {
                if (way == "left")
                {
                    while (this.Controls[name_of_shot].Left > 0) { this.Controls[name_of_shot].Left = this.Controls[name_of_shot].Left - bot_bullet_spped; await TaskEx.Delay(100); }
                }
                if (way == "right")
                {
                    while (this.Controls[name_of_shot].Left < this.Width) { this.Controls[name_of_shot].Left = this.Controls[name_of_shot].Left + bot_bullet_spped; await TaskEx.Delay(100); }
                }
                if (way == "down")
                {
                    while (this.Controls[name_of_shot].Top < this.Height) { this.Controls[name_of_shot].Top = this.Controls[name_of_shot].Top + bot_bullet_spped; await TaskEx.Delay(100); }
                }
                if (way == "up")
                {
                    while (this.Controls[name_of_shot].Top > 0) { this.Controls[name_of_shot].Top = this.Controls[name_of_shot].Top - bot_bullet_spped; await TaskEx.Delay(100); }
                }
            }
            catch { }
        }
        List<List<string>> bots = new List<List<string>>();
        int number_of_bot = 0;
        private void resp_ii()                               //респ бота
        {
            int start_x, start_y;
            Random alive = new Random();
            if (alive.Next(0, lvl_bot) == lvl_bot/2)
            {
                start_x = alive.Next(0, this.Width - this.Controls["pictureBox1"].Width) / this.Controls["pictureBox1"].Width * this.Controls["pictureBox1"].Width;
                start_y = alive.Next(0, this.Height - this.Controls["pictureBox1"].Height) / this.Controls["pictureBox1"].Height * this.Controls["pictureBox1"].Height;

                PictureBox bot = new PictureBox();
                bot.Width = 50;
                bot.Height = 50;
                bot.Left = start_x;
                bot.Top = start_y;
                bot.Visible = true;
                bot.Enabled = true;
                bot.BackgroundImage = bitmap3;
                bots.Add(new List<string>());
                bots[bots.Count - 1].Add("bots" + (number_of_bot).ToString());
                bots[bots.Count - 1].Add("up");
                bot.Name = bots[bots.Count - 1][0];
                number_of_bot++;
                Controls.Add(bot);
            }
        }

        private void timer1_Tick(object sender, EventArgs e) //респ бота и стрельба рандомного из них + уровни сложности
        {
            resp_ii();

            Random name_of_bot = new Random();
            if (bots.Count > 0)
            {
                if (name_of_bot.Next(0, lvl_bot_shot) == lvl_bot_shot/2)
                {              
                    bot_strike(name_of_bot.Next(0, bots.Count));                    
                }
            }
            if ((Convert.ToInt16(label4.Text) == 10) && (label2.Text == "0")) { lvl_bot = lvl_bot  -10; lvl_bot_shot = lvl_bot_shot - 10; label2.Text = "1"; }
            if ((Convert.ToInt16(label4.Text) == 20) && (label2.Text == "1")) { lvl_bot = lvl_bot - 10; lvl_bot_shot = lvl_bot_shot - 10; label2.Text = "2"; }
            if ((Convert.ToInt16(label4.Text) == 30) && (label2.Text == "2")) { lvl_bot = lvl_bot - 10; lvl_bot_shot = lvl_bot_shot - 10; label2.Text = "3"; }
            if ((Convert.ToInt16(label4.Text) == 40) && (label2.Text == "3")) { lvl_bot = lvl_bot - 10; lvl_bot_shot = lvl_bot_shot - 10; label2.Text = "4"; }
            if ((Convert.ToInt16(label4.Text) == 50) && (label2.Text == "4")) { lvl_bot = lvl_bot - 10; lvl_bot_shot = lvl_bot_shot - 10; label2.Text = "5"; }
            if ((Convert.ToInt16(label4.Text) == 60) && (label2.Text == "5")) { lvl_bot = lvl_bot - 10; lvl_bot_shot = lvl_bot_shot - 10; label2.Text = "6"; }
            if ((Convert.ToInt16(label4.Text) == 70) && (label2.Text == "6")) { lvl_bot = lvl_bot - 10; lvl_bot_shot = lvl_bot_shot - 10; label2.Text = "7"; }
            if ((Convert.ToInt16(label4.Text) == 80) && (label2.Text == "7")) { lvl_bot = lvl_bot - 10; lvl_bot_shot = lvl_bot_shot - 10; label2.Text = "8"; }
            if ((Convert.ToInt16(label4.Text) == 90) && (label2.Text == "8")) { lvl_bot = lvl_bot - 10; lvl_bot_shot = lvl_bot_shot - 10; label2.Text = "9"; }
            if ((Convert.ToInt16(label4.Text) == 100) && (label2.Text == "9")) { lvl_bot = lvl_bot - 10; lvl_bot_shot = lvl_bot_shot -10; label2.Text = "GOD MODE"; }
        }

        private void timer4_Tick(object sender, EventArgs e) //убит перс
        {
            int j;
            try
            {
                for (j = 0; j < bot_shots.Count; j++)
                {
                    if (((this.Controls["pictureBox1"].Top + this.Controls["pictureBox1"].Height) >= (this.Controls[bot_shots[j]].Top))
                        && ((this.Controls["pictureBox1"].Top) <= (this.Controls[bot_shots[j]].Top))
                        && ((this.Controls["pictureBox1"].Left) <= (this.Controls[bot_shots[j]].Left))
                        && ((this.Controls["pictureBox1"].Left + this.Controls["pictureBox1"].Width) >= (this.Controls[bot_shots[j]].Left)))
                    {
                        this.Controls.RemoveByKey(bot_shots[j]);
                        bot_shots.Remove(bot_shots[j]);
                        destroyed = true;
                        button1.Visible = true;
                        button1.Enabled = true;
                        timer1.Enabled = false;
                        timer2.Enabled = false;
                        timer3.Enabled = false;
                        this.Controls["pictureBox1"].BackgroundImage = bitmap7;
                        timer4.Enabled = false;
                    }
                }
            }
            catch { }
        }

        private void resp() //создать персонажа
        {
            black_square.height = 50;
            black_square.widght = 50;
            black_square.speed = 50;
            last_tab = "up";
            bitmap1 = Properties.Resources._50x50;
            bitmap2 = Properties.Resources._50x50_2; //pers shooting
            bitmap7 = Properties.Resources._50x50_dead; //pers dead

            this.Controls["pictureBox1"].BackgroundImage = bitmap1;
            pictureBox1.Height = black_square.height;
            pictureBox1.Width = black_square.widght;
            Random rand = new Random();
            int start_x, start_y;
            start_x = rand.Next(0, this.Width - this.Controls["pictureBox1"].Width) / this.Controls["pictureBox1"].Width * this.Controls["pictureBox1"].Width;
            start_y = rand.Next(0, this.Height - this.Controls["pictureBox1"].Height) / this.Controls["pictureBox1"].Height * this.Controls["pictureBox1"].Height;
            this.Controls["pictureBox1"].Left = start_x;
            this.Controls["pictureBox1"].Top = start_y;
        }
        private void button1_Click(object sender, EventArgs e) //restart
        {
            for (int i = 0; i < bots.Count; i++) this.Controls.RemoveByKey(bots[i][0]);
            for (int j = 0; j < bot_shots.Count; j++) this.Controls.RemoveByKey(bot_shots[j]);
            for (int k = 0; k < bs_shots.Count; k++) this.Controls.RemoveByKey(bs_shots[k]);            
            bots.Clear();
            bot_shots.Clear();
            bs_shots.Clear();
            bot_number_of_shot = 0;
            number_of_shot = 0;
            number_of_bot = 0;
            label4.Text = "0";
            resp();
            label2.Text = "0";
            lvl_bot = 100;
            lvl_bot_shot = 100;
            timer1.Enabled = true;
            timer2.Enabled = true;
            timer3.Enabled = true;
            timer4.Enabled = true;
            destroyed = false;
            button1.Visible = false;
            this.Activate();
            button1.Enabled = false;
        }


        private async void timer2_Tick(object sender, EventArgs e) //убарть убитого бота и пулю
        {
            int i, j;
            for (i = 0; i < bots.Count; i++)
            {
                for (j = 0; j < bs_shots.Count; j++)
                {
                    try
                    {

                        if (((this.Controls[bots[i][0]].Top + this.Controls[bots[i][0]].Height) >= (this.Controls[bs_shots[j]].Top))
                            && ((this.Controls[bots[i][0]].Top) <= (this.Controls[bs_shots[j]].Top))
                            && ((this.Controls[bots[i][0]].Left) <= (this.Controls[bs_shots[j]].Left))
                            && ((this.Controls[bots[i][0]].Left + this.Controls[bots[i][0]].Width) >= (this.Controls[bs_shots[j]].Left)))
                        {
                            this.Controls.RemoveByKey(bs_shots[j]);
                            bs_shots.Remove(bs_shots[j]);
                            Bitmap bitmap9 = new Bitmap(bitmap8);
                            if (bots[i][1] == "right")  { bitmap9.RotateFlip(RotateFlipType.Rotate90FlipNone); }
                            if (bots[i][1] == "left") { bitmap9.RotateFlip(RotateFlipType.Rotate270FlipNone); }
                            if (bots[i][1] == "down") { bitmap9.RotateFlip(RotateFlipType.Rotate180FlipNone); }
                            this.Controls[bots[i][0]].BackgroundImage = bitmap9;
                            await TaskEx.Delay(100);
                            this.Controls.RemoveByKey(bots[i][0]);
                            bots.Remove(bots[i]);
                            label4.Text = (Convert.ToInt64(label4.Text) + 1).ToString();
                        }
                    }
                    catch { }
                }
            }

        }

        private void timer3_Tick(object sender, EventArgs e) //убрать улетевшую пулю перса и бота
        {
            for (int i = 0; i < bs_shots.Count; i++)
            {
                if ((this.Controls[bs_shots[i]].Top <= 0) || (this.Controls[bs_shots[i]].Left <= 0) || (this.Controls[bs_shots[i]].Left >= this.Width) || (this.Controls[bs_shots[i]].Top >= this.Height))
                {
                    this.Controls.RemoveByKey(bs_shots[i]);
                    bs_shots.Remove(bs_shots[i]);
                }
            }

            for (int j = 0; j < bot_shots.Count; j++)
            {
                if ((this.Controls[bot_shots[j]].Top <= 0) || (this.Controls[bot_shots[j]].Left <= 0) || (this.Controls[bot_shots[j]].Left >= this.Width) || (this.Controls[bot_shots[j]].Top >= this.Height))
                {
                    this.Controls.RemoveByKey(bot_shots[j]);
                    bot_shots.Remove(bot_shots[j]);
                }
            }
        }
    }
}
